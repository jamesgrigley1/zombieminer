#pragma once
#include "Utility.h"

class Block
{
public:
	int xPos;
	int yPos;
	ColorRGB::Color color;

	Block();
	~Block();
	Block(int _x, int _y);


	enum BlockType{
		EmptyAir = 1,
		Dirt = 2,
		Rock = 3,
		Water = 4
	};

	std::string blockName;

	BlockType blockType;

	void setType(BlockType _blockType);

	void start();
	void update();
	void draw();
};

