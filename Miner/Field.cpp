#include "stdafx.h"
#include "Field.h"
#include "Utility.h"
#include "Block.h"


Field::Field()
{
	
}


Field::~Field()
{
}

void Field::loadHorizon(){
	int middlePoint = BLOCKSPERCOL / 2;

	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y <= middlePoint; y++){
			//set block to dirt
			if (x != 28)
				fieldBlocks[x][y]->setType(Block::BlockType::Dirt);
		}
	}
}
void Field::loadBlocks(){
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			fieldBlocks[x][y] = new Block(FIELDX + (x * BLOCKWIDTH), FIELDY + (y * BLOCKHEIGHT));
			//fieldBlocks[x][y] = new Block();
		}
	}
	loadHorizon();
}
void Field::drawBlocks(){
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			if (fieldBlocks[x][y]->color >= 0){
				fieldBlocks[x][y]->draw();
			}
		}
	}
}

int Field::collidesWithField(int _x, int _y, int direction){

	// then see if player collides with block in field
	for (int x = 0; x < BLOCKSPERROW; x++){
		for (int y = 0; y < BLOCKSPERCOL; y++){
			//collides with other blocks
			if (direction == 0
				&& fieldBlocks[x][y]->blockType != Block::BlockType::EmptyAir
				&& (_y - 1) < fieldBlocks[x][y]->yPos + BLOCKHEIGHT // fell below horisontal
				&& _x >= fieldBlocks[x][y]->xPos // is within left vertiacal bound
				&& _x < fieldBlocks[x][y]->xPos + BLOCKWIDTH //is within right vertiacal bound
				){
				player->yPos = fieldBlocks[x][y]->yPos + BLOCKHEIGHT;
				return static_cast<int>(CollisionType::Down);
			}
			else if (direction == 2
				&& fieldBlocks[x][y]->blockType != Block::BlockType::EmptyAir
				&& (_y - 1) < fieldBlocks[x][y]->yPos + BLOCKHEIGHT // fell below horizontal
				&& _x >= fieldBlocks[x][y]->xPos // is within left vertiacal bound
				&& _x < fieldBlocks[x][y]->xPos + BLOCKWIDTH //is within right vertiacal bound
				){
				player->yPos = fieldBlocks[x][y]->yPos + BLOCKHEIGHT;
				return static_cast<int>(CollisionType::Down);
			}
		}
	}

	return static_cast<int>(CollisionType::None);
}

int Field::checkCollision(int direction){
	CollisionType collisionType;
	collisionType = static_cast<CollisionType>(collidesWithField(player->xPos, player->yPos, direction));
	if (collisionType != CollisionType::None){
		return static_cast<int>(collisionType);
	}
	return 0;
}


void Field::start(){
	loadBlocks();
	player = new Player(30,60, 200, 800);
}

void Field::update(int direction){
	CollisionType collisionType = static_cast < CollisionType > (checkCollision(direction));
	//if (collisionType == CollisionType::Down){
	//	addShapeToBlocks();
	//	checkLines();
	//	currentShape = new Shape(speed);
	//}
	if (collisionType == CollisionType::None){
		player->update(direction);
	}
}

void Field::draw(){
	Utility::drawRect(FIELDX, FIELDY, BLOCKWIDTH * BLOCKSPERROW, BLOCKHEIGHT * BLOCKSPERCOL, 1);
	//currentShape->draw();
	drawBlocks();
	player->draw();
}

