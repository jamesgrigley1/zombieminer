#pragma once
#include "block.h"
#include "player.h"
#define BLOCKWIDTH 30
#define BLOCKHEIGHT 30
#define BLOCKSPERROW 45
#define BLOCKSPERCOL 25
#define FIELDX 100
#define FIELDY 100


class Field
{
public:
	Block *fieldBlocks[BLOCKSPERROW][BLOCKSPERCOL];
	Player *player;

	enum CollisionType{
		None = 0,
		Down = 1,
		Side = 2
	};

	Field();
	~Field();

	void loadBlocks();
	void loadHorizon();
	void drawBlocks();
	int checkCollision(int direction);
	int collidesWithField(int _x, int _y, int direction);

	void start();

	void update(int direction);

	void draw();
};

